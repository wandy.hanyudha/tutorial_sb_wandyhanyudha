package com.wandy.hanyudha.pelatihan.aplikasipelatihan.dao;

import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Peserta;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

@SpringBootTest
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/data/peserta.sql"
)
public class PesertaDaoTest {
    @Autowired
    private PesertaDao pd;

    @Autowired
    private DataSource ds;

    @AfterEach
    public void hapusData() throws Exception{
        String sql = "DELETE FROM peserta WHERE email = 'Peserta003@gmail.com'";
        try (Connection c = ds.getConnection()) {
            c.createStatement().executeUpdate(sql);
        }
    }

    @Test
    public void testInsert() throws SQLException {
        Peserta p = new Peserta();
        p.setNama("Peserta 003");
        p.setEmail("Peserta003@gmail.com");
        p.setTanggalLahir(new Date());

        pd.save(p);

        String sql = "SELECT COUNT(*) AS jumlah FROM peserta WHERE email = 'Peserta003@gmail.com'";

        try (Connection c = ds.getConnection()) {
            ResultSet rs = c.createStatement().executeQuery(sql);
            Assertions.assertTrue(rs.next());

            Long jumlahRow = rs.getLong("jumlah");
            Assertions.assertEquals(1L, jumlahRow.longValue());
        }
    }

    @Test
    public void testHitung(){
        Long jumlah = pd.count();
        Assertions.assertEquals(3L, jumlah.longValue());
    }

    @Test
    public void testCariById(){
        Optional<Peserta> p;
        p = pd.findById("aa");
        Assertions.assertNotNull(p);
        Assertions.assertEquals(p.get().getNama(), "peserta test 001");
        Assertions.assertEquals(p.get().getEmail(), "peserta.test.001@gmail.com");


    }



}
