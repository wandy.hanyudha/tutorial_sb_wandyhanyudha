package com.wandy.hanyudha.pelatihan.aplikasipelatihan.dao;

import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Materi;
import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Peserta;
import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Sesi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.jdbc.Sql;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@SpringBootTest
@Sql(
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"/data/peserta.sql", "/data/materi.sql", "/data/sesi.sql"}
)
public class SesiDaoTest {

    @Autowired
    private SesiDao sd;

    @Autowired
    private DataSource ds;

    @Test
    public void testCariByMateri(){
        Materi m = new Materi();
        m.setId("aa6");

        Pageable page = PageRequest.of(0, 2, Sort.Direction.ASC, "id");
        //Pageable page = new PageRequest(0,5);
        Page<Sesi> hasilQuery = sd.findByMateri(m, page);
        Assertions.assertEquals(2L, hasilQuery.getTotalElements());

        Assertions.assertFalse(hasilQuery.getContent().isEmpty());
        Sesi s = hasilQuery.getContent().get(0);
        Assertions.assertNotNull(s);
        Assertions.assertEquals("Java Fundamental", s.getMateri().getNama());
    }

    @Test
    public void testCariBerdasarkanTanggalMulaidanKodeMateri() throws ParseException {
        Pageable page = PageRequest.of(0, 2, Sort.Direction.ASC, "id");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date sejak = formatter.parse("2015-01-01");
        Date sampai = formatter.parse("2015-01-03");

        Page<Sesi> hasil = sd.cariBerdasarkanTanggalMulaidanKodeMateri(sejak, sampai, "JF-002", page);
        Assertions.assertEquals(1L, hasil.getTotalElements());
        Assertions.assertFalse(hasil.getContent().isEmpty());

        Sesi s = hasil.getContent().get(0);
        Assertions.assertEquals("How To Be A GGWP DOTA Player", s.getMateri().getNama());
    }

    @Test
    public void testSaveSesi() throws ParseException, SQLException{
        Peserta p1 = new Peserta();
        p1.setId("aa");

        Peserta p2 = new Peserta();
        p2.setId("ab");

        Materi m = new Materi();
        m.setId("aa8");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date sejak = formatter.parse("2015-02-01");
        Date sampai = formatter.parse("2015-02-03");

        Sesi s = new Sesi();
        s.setMateri(m);
        s.setMulai(sejak);
        s.setSampai(sampai);
        s.getDaftarPeserta().add(p1);
        s.getDaftarPeserta().add(p2);
        sd.save(s);
        String idSesiBaru = s.getId();
        Assertions.assertNotNull(idSesiBaru);
        System.out.println("ID Baru: " + s.getId());

        String sql = "select count(*) from sesi where id_materi='aa8'";
        String sqlManyToMany = "select count(*) from peserta_pelatihan "
                            + "where id_sesi=?";
        try (Connection c = ds.getConnection()){
            //cek tabel sesi
            ResultSet rs = c.createStatement().executeQuery(sql);
            Assertions.assertTrue(rs.next());
            Assertions.assertEquals(1L, rs.getLong(1));

            //cek tabel relasi many to many dengan peserta
            PreparedStatement ps = c.prepareStatement(sqlManyToMany);
            ps.setString(1, idSesiBaru);
            ResultSet rs2 = ps.executeQuery();

            Assertions.assertTrue(rs2.next());
            Assertions.assertEquals(2L, rs2.getLong(1));
        }
    }
}
