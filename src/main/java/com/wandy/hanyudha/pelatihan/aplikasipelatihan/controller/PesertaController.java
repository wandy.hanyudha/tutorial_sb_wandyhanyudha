package com.wandy.hanyudha.pelatihan.aplikasipelatihan.controller;

import com.wandy.hanyudha.pelatihan.aplikasipelatihan.dao.PesertaDao;
import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Peserta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class PesertaController {

    @Autowired
    private PesertaDao pd;

    @RequestMapping(value = "/peserta", method = RequestMethod.GET)
    public Page<Peserta> cariPeserta(Pageable page){
        return pd.findAll(page);
    }

    @RequestMapping(value = "/peserta", method = RequestMethod.POST)
    //@ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<Peserta> insertPesertaBaru(@Valid @RequestBody Peserta p){
        Peserta peserta = this.pd.save(p);
        return new ResponseEntity<>(peserta, HttpStatus.OK);
        //pd.save(p);
    }

    @RequestMapping(value = "/peserta/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public String updatePeserta(@PathVariable("id") String id, @Valid  @RequestBody Peserta p){
        p.setId(id);
        pd.save(p);

        return "/peserta/list";
    }


    @GetMapping(path = "/peserta/{id}")
    public ResponseEntity<Peserta> cariPesertaById(@PathVariable String id){
        Optional<Peserta> pesertaOpt = pd.findById(id);
        return pesertaOpt.map(peserta -> new ResponseEntity<>(peserta, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }



/*    @GetMapping("/peserta/{id}")
    public Peserta cariPesertaById(@PathVariable String id){
        Optional<Peserta> pesertaOpt = pd.findById(id);
        if(pesertaOpt.isPresent()){
            return pesertaOpt.get();
        }
        else {
            throw new RuntimeException("Peserta tidak ditemukan dengan id: " + id );
        }
    }*/

 /*   @RequestMapping(value = "/peserta/{id}", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    public Optional<Peserta> cariPesertaById(@PathVariable("id") String id){
        return pd.findById(id);
    }*/

    @RequestMapping(value = "/peserta/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void hapusPeserta(@PathVariable("id") String id){
        pd.deleteById(id);
    }
}
