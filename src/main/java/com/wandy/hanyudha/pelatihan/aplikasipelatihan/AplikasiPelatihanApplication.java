package com.wandy.hanyudha.pelatihan.aplikasipelatihan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class AplikasiPelatihanApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiPelatihanApplication.class, args);
	}

}
