package com.wandy.hanyudha.pelatihan.aplikasipelatihan.dao;

import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Materi;
import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Sesi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface SesiDao extends PagingAndSortingRepository<Sesi, String> {
    Page<Sesi> findByMateri(Materi m, Pageable page);


    @Query("SELECT x FROM Sesi x WHERE x.mulai >= :m "
            + "AND x.mulai < :s "
            + "AND x.materi.kode = :k "
            + "ORDER BY x.mulai DESC ")
    Page<Sesi> cariBerdasarkanTanggalMulaidanKodeMateri(
            @Param("m") Date mulai,
            @Param("s") Date sampai,
            @Param("k") String kode,
            Pageable page
    );

}
