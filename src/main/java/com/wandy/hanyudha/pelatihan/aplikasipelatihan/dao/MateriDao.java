package com.wandy.hanyudha.pelatihan.aplikasipelatihan.dao;

import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Materi;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MateriDao extends PagingAndSortingRepository<Materi, String> {
}
