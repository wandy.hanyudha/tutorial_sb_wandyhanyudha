package com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity;
//import com.sun.istack.NotNull;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Peserta {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(nullable = false)
    @NotNull(message = "Masukkan Nama")
    @NotEmpty (message = "Nama tidak boleh kosong, bro/sis!")
    @Size(min = 3, max = 150, message = "Nama minimal 3 karakter dan maksimal 150 karakter")
    private String nama;

    @Column(nullable = false, unique = true)
    @Email()
    @NotNull (message = "Tidak boleh kosong")
    @NotEmpty (message = "Tidak boleh kosong")
    private String email;

    @Column(name = "tanggal_lahir", nullable = false)
    @Past(message = "Tanggal lahirnya melebihi dari tanggal sekarang, kan ga mungkin bro!")
    @NotNull(message = "Tanggal tidak boleh kosong, om!")
    @Temporal(TemporalType.DATE)
    private Date tanggalLahir;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}
