package com.wandy.hanyudha.pelatihan.aplikasipelatihan.dao;

import com.wandy.hanyudha.pelatihan.aplikasipelatihan.entity.Peserta;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {
}
